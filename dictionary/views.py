# views.py
import re
from django.shortcuts import render, redirect
from django.contrib.admin.views.decorators import staff_member_required
from .models import Word, Translation, RussianWordVariant
from django.core.files.uploadedfile import UploadedFile
from django.contrib import messages
import docx


@staff_member_required
def import_dictionary_view(request):
    if request.method == 'POST':
        files = request.FILES.getlist('docx_files')
        selected_letter = request.POST.get('letter')

        if not files:
            messages.error(request, "Пожалуйста, выберите хотя бы один файл.")
            return render(request, 'admin/import_dictionary.html', {'letters': Translation.LETTER_CHOICES})

        if not selected_letter:
            messages.error(request, "Пожалуйста, выберите букву для импорта.")
            return render(request, 'admin/import_dictionary.html', {'letters': Translation.LETTER_CHOICES})

        for docx_file in files:
            if not docx_file.name.endswith('.docx'):
                messages.error(request, f"Файл {docx_file.name} не является файлом .docx. Пропускаем.")
                continue
            try:
                process_docx_file(docx_file, selected_letter)
                messages.success(request, f"Файл {docx_file.name} успешно обработан для буквы {selected_letter}.")
            except Exception as e:
                messages.error(request, f"Ошибка при обработке файла {docx_file.name}: {str(e)}")

        return redirect('base')

    return render(request, 'admin/import_dictionary.html', {'letters': Translation.LETTER_CHOICES})


def process_docx_file(docx_file: UploadedFile, selected_letter: str):
    doc = docx.Document(docx_file)
    lines = [para.text for para in doc.paragraphs if para.text.strip()]

    for line in lines:
        line = line.strip().lower()
        parts = re.split(r'\s*-\s*', line)
        if len(parts) < 2:
            continue

        dargin_words = [t.strip() for t in parts[0].split(',')]
        russian_variants = [t.strip() for t in parts[1].split(',')]

        word_instance = Word.objects.create()

        for word in dargin_words:
            word = re.sub(r'[.,]', '', word)
            word = word.strip().lower()
            Translation.objects.create(
                word=word_instance,
                dargin_translation=word,
                letter=selected_letter
            )

        variant_added = False
        for variant in russian_variants:
            variant = re.sub(r'[.,]', '', variant)
            variant = variant.strip().lower()
            if not RussianWordVariant.objects.filter(variant=variant).exists():
                RussianWordVariant.objects.create(
                    word=word_instance,
                    variant=variant
                )
                variant_added = True

        if not variant_added:
            word_instance.delete()


@staff_member_required
def delete_base_view(request):
    if request.method == 'POST':
        selected_letter = request.POST.get('letter')
        if selected_letter:
            # Подсчитываем количество объектов до удаления
            translations_count = Translation.objects.filter(letter=selected_letter).count()
            words_to_delete = Word.objects.filter(translations__letter=selected_letter)
            words_count = words_to_delete.count()
            variants_count = RussianWordVariant.objects.filter(word__translations__letter=selected_letter).count()

            # Удаляем Word объекты (каскадно удалятся все связанные объекты)
            words_to_delete.delete()

            messages.success(
                request,
                f'Успешно удалено для буквы {selected_letter}:\n'
                f'- {words_count} слов\n'
                f'- {translations_count} переводов\n'
                f'- {variants_count} русских вариантов'
            )
        else:
            messages.error(request, 'Выберите букву для удаления')

        return redirect('base')

    context = {
        'letters': Translation.LETTER_CHOICES
    }
    return render(request, 'admin/delete_base.html', context)


@staff_member_required
def base_view(request):
    context = {
        'word_count': Word.objects.count(),
        'translation_count': Translation.objects.count(),
        'variant_count': RussianWordVariant.objects.count(),
    }
    return render(request, 'admin/base_page.html', context)