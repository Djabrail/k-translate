import os
import django
from django.core.management.base import BaseCommand
from django.conf import settings
from telegram import Update, InlineKeyboardButton, InlineKeyboardMarkup, BotCommand
from telegram.ext import ApplicationBuilder, CommandHandler, CallbackContext, MessageHandler, filters, CallbackQueryHandler
import asyncio
from asgiref.sync import sync_to_async
from fuzzywuzzy import fuzz, process

# Установка переменной окружения для настройки Django
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'translate.settings')

# Инициализация Django
django.setup()

from dictionary.models import Word, RussianWordVariant, Translation

class Command(BaseCommand):
    help = 'Runs the Telegram bot'

    async def start(self, update: Update, context: CallbackContext) -> None:
        keyboard = [
            [
                InlineKeyboardButton("Русский → Аштынский", callback_data='ru_to_ash'),
                InlineKeyboardButton("Аштынский → Русский", callback_data='ash_to_ru'),
            ]
        ]
        reply_markup = InlineKeyboardMarkup(keyboard)
        await update.message.reply_text(
            'Ас-саляму алейкум! Выберите направление перевода:',
            reply_markup=reply_markup
        )

    async def help(self, update: Update, context: CallbackContext) -> None:
        help_text = (
            "Этот бот переводит слова с русского на аштынский и наоборот.\n\n"
            "Доступные команды:\n"
            "/start - Начать использование бота\n"
            "/help - Получить эту справку\n"
            "/ru_to_ash - Установить направление перевода с русского на аштынский\n"
            "/ash_to_ru - Установить направление перевода с аштынского на русский\n\n"
            "После выбора направления перевода просто отправьте боту слово для перевода."
        )
        await update.message.reply_text(help_text)

    async def ru_to_ash(self, update: Update, context: CallbackContext) -> None:
        context.user_data['translation_direction'] = 'ru_to_ash'
        await update.message.reply_text("Направление перевода установлено: с русского на аштынский. Отправьте слово для перевода.")

    async def ash_to_ru(self, update: Update, context: CallbackContext) -> None:
        context.user_data['translation_direction'] = 'ash_to_ru'
        await update.message.reply_text("Направление перевода установлено: с аштынского на русский. Отправьте слово для перевода.")

    async def button(self, update: Update, context: CallbackContext) -> None:
        query = update.callback_query
        await query.answer()

        direction = query.data
        context.user_data['translation_direction'] = direction

        if direction == 'ru_to_ash':
            text = "Направление перевода установлено: с русского на аштынский. Отправьте слово для перевода."
        else:
            text = "Направление перевода установлено: с аштынского на русский. Отправьте слово для перевода."

        await query.edit_message_text(text=text)

    async def translate(self, update: Update, context: CallbackContext) -> None:
        if 'translation_direction' not in context.user_data:
            keyboard = [
                [
                    InlineKeyboardButton("Русский → Аштынский", callback_data='ru_to_ash'),
                    InlineKeyboardButton("Аштынский → Русский", callback_data='ash_to_ru'),
                ]
            ]
            reply_markup = InlineKeyboardMarkup(keyboard)
            await update.message.reply_text(
                "Пожалуйста, сначала выберите направление перевода:",
                reply_markup=reply_markup
            )
            return

        word = update.message.text.strip().lower()
        direction = context.user_data['translation_direction']

        if direction == 'ru_to_ash':
            await self.translate_ru_to_ash(update, word)
        else:
            await self.translate_ash_to_ru(update, word)

    async def translate_ru_to_ash(self, update: Update, russian_word: str) -> None:
        all_variants = await sync_to_async(list)(RussianWordVariant.objects.all())
        best_match, score = process.extractOne(russian_word, [v.variant for v in all_variants], scorer=fuzz.ratio)
        
        if score >= 80:
            variant = next(v for v in all_variants if v.variant == best_match)
            word = await sync_to_async(lambda: variant.word)()
            translations = await sync_to_async(lambda: list(word.translations.all()))()
            
            if translations:
                response = f"Переводы слова '{best_match}':\n"
                response += "\n".join([f"{idx + 1}. {t.dargin_translation}" for idx, t in enumerate(translations)])
                
                if best_match != russian_word:
                    response = f"Возможно, вы имели в виду '{best_match}'?\n\n" + response
            else:
                response = "Переводы не найдены."
        else:
            response = f"Перевод для '{russian_word}' не найден. Проверьте правильность написания."
        
        await update.message.reply_text(response)

    async def translate_ash_to_ru(self, update: Update, dargin_word: str) -> None:
        all_translations = await sync_to_async(list)(Translation.objects.all())
        best_match, score = process.extractOne(dargin_word, [t.dargin_translation for t in all_translations], scorer=fuzz.ratio)
        
        if score >= 80:
            translation = next(t for t in all_translations if t.dargin_translation == best_match)
            word = await sync_to_async(lambda: translation.word)()
            russian_variants = await sync_to_async(lambda: list(word.russian_variants.all()))()
            
            if russian_variants:
                response = f"Переводы слова '{best_match}':\n"
                response += "\n".join([f"{idx + 1}. {v.variant}" for idx, v in enumerate(russian_variants)])
                
                if best_match != dargin_word:
                    response = f"Возможно, вы имели в виду '{best_match}'?\n\n" + response
            else:
                response = "Переводы не найдены."
        else:
            response = f"Перевод для '{dargin_word}' не найден. Проверьте правильность написания."
        
        await update.message.reply_text(response)

    async def setup_commands(self, application):
        commands = [
            BotCommand("start", "Начать использование бота"),
            BotCommand("help", "Получить справку"),
            BotCommand("ru_to_ash", "Перевод с русского на аштынский"),
            BotCommand("ash_to_ru", "Перевод с аштынского на русский"),
        ]
        await application.bot.set_my_commands(commands)

    async def run_bot(self):
        app = ApplicationBuilder().token(settings.BOT_TOKEN).build()

        app.add_handler(CommandHandler("start", self.start))
        app.add_handler(CommandHandler("help", self.help))
        app.add_handler(CommandHandler("ru_to_ash", self.ru_to_ash))
        app.add_handler(CommandHandler("ash_to_ru", self.ash_to_ru))
        app.add_handler(CallbackQueryHandler(self.button))
        app.add_handler(MessageHandler(filters.TEXT & ~filters.COMMAND, self.translate))
        
        await app.initialize()
        await app.start()
        await self.setup_commands(app)
        print("Bot is running...")
        await app.updater.start_polling()

    def handle(self, *args, **options):
        loop = asyncio.get_event_loop()
        loop.create_task(self.run_bot())

        try:
            loop.run_forever()
        except KeyboardInterrupt:
            pass
        finally:
            tasks = asyncio.all_tasks(loop)
            for task in tasks:
                task.cancel()
            loop.run_until_complete(asyncio.gather(*tasks, return_exceptions=True))
            loop.close()