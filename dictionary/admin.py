from django.contrib import admin
from .models import Word, Translation, RussianWordVariant
from django.contrib import messages

class RussianWordVariantInline(admin.TabularInline):
    model = RussianWordVariant
    extra = 1

class TranslationInline(admin.TabularInline):
    model = Translation
    extra = 1

class WordAdmin(admin.ModelAdmin):
    inlines = [RussianWordVariantInline, TranslationInline]
    actions = ['delete_all_words']

    def delete_all_words(self, request, queryset):
        """
        Удаляет все объекты Word из базы данных.
        """
        if request.user.is_superuser:  # Проверяем, что пользователь администратор
            Word.objects.all().delete()
            messages.success(request, "Все слова были успешно удалены.")
        else:
            messages.error(request, "У вас нет прав для выполнения этого действия.")
    
    delete_all_words.short_description = "Удалить все слова"

admin.site.register(Word, WordAdmin)
admin.site.register(Translation)
admin.site.register(RussianWordVariant)