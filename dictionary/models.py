from django.db import models

class Word(models.Model):
    def __str__(self):
        variants = ", ".join([variant.variant for variant in self.russian_variants.all()])
        return f"{variants}"

class RussianWordVariant(models.Model):
    word = models.ForeignKey(Word, on_delete=models.CASCADE, related_name='russian_variants')
    variant = models.CharField(max_length=100)

    def __str__(self):
        return self.variant

class Translation(models.Model):
    LETTER_CHOICES = [
        ('А', 'А'), ('Б', 'Б'), ('В', 'В'), ('Г', 'Г'), ('Д', 'Д'),
        ('Е', 'Е'), ('Ё', 'Ё'), ('Ж', 'Ж'), ('З', 'З'), ('И', 'И'),
        ('Й', 'Й'), ('К', 'К'), ('Л', 'Л'), ('М', 'М'), ('Н', 'Н'),
        ('О', 'О'), ('П', 'П'), ('Р', 'Р'), ('С', 'С'), ('Т', 'Т'),
        ('У', 'У'), ('Ф', 'Ф'), ('Х', 'Х'), ('Ц', 'Ц'), ('Ч', 'Ч'),
        ('Ш', 'Ш'), ('Щ', 'Щ'), ('Ъ', 'Ъ'), ('Ы', 'Ы'), ('Ь', 'Ь'),
        ('Э', 'Э'), ('Ю', 'Ю'), ('Я', 'Я'),
    ]
    word = models.ForeignKey(Word, on_delete=models.CASCADE, related_name='translations')
    dargin_translation = models.CharField(max_length=100)
    letter = models.CharField(max_length=1, choices=LETTER_CHOICES, default='А')

    def __str__(self):
        return self.dargin_translation
